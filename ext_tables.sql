#
# Table structure for table 'tx_dashboard_domain_model_dashboardwidget'
#
CREATE TABLE tx_dashboard_domain_model_dashboardwidget (

	feed_url varchar(255) DEFAULT '' NOT NULL,
	feed_limit int(11) DEFAULT '0' NOT NULL,

	tx_extbase_type varchar(255) DEFAULT '' NOT NULL,

);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder