<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Dashboard Wídget RSS');

if (!isset($GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['ctrl']['dynamicConfigFile']);
	}
	// no type field defined, so we define it here. This will only happen the first time the extension is installed!!
	$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['ctrl']['type'] = 'tx_extbase_type';
	$tempColumns = array();
	$tempColumns[$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['ctrl']['type']] = array(
		'exclude' => 1,
		'label'   => 'LLL:EXT:dashboard_widget_rss/Resources/Private/Language/locallang_db.xlf:tx_dashboardwidgetrss.tx_extbase_type',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('Default','1')
			),
			'size' => 1,
			'maxitems' => 1,
		)
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_dashboard_domain_model_dashboardwidget', $tempColumns, 1);
}

$tmp_dashboard_widget_rss_columns = array(

	'feed_url' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:dashboard_widget_rss/Resources/Private/Language/locallang_db.xlf:tx_dashboardwidgetrss_domain_model_dashboardwidgetrss.feed_url',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim,required'
		),
	),
	'feed_limit' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:dashboard_widget_rss/Resources/Private/Language/locallang_db.xlf:tx_dashboardwidgetrss_domain_model_dashboardwidgetrss.feed_limit',
		'config' => array(
			'type' => 'input',
			'size' => 4,
			'eval' => 'int'
		)
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_dashboard_domain_model_dashboardwidget',$tmp_dashboard_widget_rss_columns);

$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['types']['Tx_DashboardWidgetRss_DashboardWidgetRss']['showitem'] = $TCA['tx_dashboard_domain_model_dashboardwidget']['types']['1']['showitem'];
$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['types']['Tx_DashboardWidgetRss_DashboardWidgetRss']['showitem'] .= ',--div--;LLL:EXT:dashboard_widget_rss/Resources/Private/Language/locallang_db.xlf:tx_dashboardwidgetrss_domain_model_dashboardwidgetrss,';
$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['types']['Tx_DashboardWidgetRss_DashboardWidgetRss']['showitem'] .= 'feed_url, feed_limit';

$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['columns'][$TCA['tx_dashboard_domain_model_dashboardwidget']['ctrl']['type']]['config']['items'][] = array(
	'LLL:EXT:dashboard_widget_rss/Resources/Private/Language/locallang_db.xlf:tx_dashboard_domain_model_dashboardwidget.tx_extbase_type.Tx_DashboardWidgetRss_DashboardWidgetRss',
	'Tx_DashboardWidgetRss_DashboardWidgetRss'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'tx_dashboard_domain_model_dashboardwidget',
	'EXT:dashboard_widget_rss/Resources/Private/Language/locallang_csh_tx_dashboard_domain_model_dashboardwidget.xlf'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_dashboard_domain_model_dashboardwidget',
	$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidget']['ctrl']['type'],
	'',
	'after:' . $TCA['tx_dashboard_domain_model_dashboardwidget']['ctrl']['label']
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder