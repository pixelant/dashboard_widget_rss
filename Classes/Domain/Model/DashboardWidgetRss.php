<?php
namespace Pixelant\DashboardWidgetRss\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * DashboardWidgetRss
 */
class DashboardWidgetRss extends \TYPO3\CMS\Dashboard\Domain\Model\DashboardWidget {

	/**
	 * Feed URL
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $feedUrl = '';

	/**
	 * Limit, If set, it will limit the results in the list.
	 *
	 * @var integer
	 */
	protected $feedLimit = 0;

	public function initialize() {
		// Set cacheLifetime so the content will get cached
		$this->cacheLifetime = 3600;
	}

	/**
	 * generateWidgetContent Generates the actual content to render
	 * @return void
	 */
	public function generateWidgetContent() {
		/** @var \TYPO3\CMS\Fluid\View\StandaloneView $emailView */
		$rssView = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager')->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
		$template = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:dashboard_widget_rss/Resources/Private/Templates/Rss.html');
		$rssView->setTemplatePathAndFilename($template);

		$feed = $this->getFeed();
		$rssView->assign('feed', $feed);

		$this->content = $rssView->render();
	}

	/**
	 * Returns the feedUrl
	 *
	 * @return string feedUrl
	 */
	public function getFeedUrl() {
		return $this->feedUrl;
	}

	/**
	 * Sets the feedUrl
	 *
	 * @param string $feedUrl
	 * @return string feedUrl
	 */
	public function setFeedUrl($feedUrl) {
		$this->feedUrl = $feedUrl;
	}

	/**
	 * Returns the feedLimit
	 *
	 * @return integer $feedLimit
	 */
	public function getFeedLimit() {
		return $this->feedLimit;
	}

	/**
	 * Sets the feedLimit
	 *
	 * @param integer $feedLimit
	 * @return void
	 */
	public function setFeedLimit($feedLimit) {
		$this->feedLimit = $feedLimit;
	}

	/**
	 * Loads feed and cuts unneeded items
	 *
	 * @return array Array from xml
	 */
	protected function getFeed() {
		$feedTimeout = 10;
		$feed = array();
		if (\TYPO3\CMS\Core\Utility\GeneralUtility::isValidUrl($this->feedUrl)) {
			try {
				$request = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
					'TYPO3\\CMS\\Core\\Http\\HttpRequest',
					$this->feedUrl,
					\TYPO3\CMS\Core\Http\HttpRequest::METHOD_GET,
					array(
						'timeout' => $feedTimeout,
						'follow_redirects' => true
					)
				);
				$result = $request->send();
				$content = $result->getBody();
				$simpleXmlElement = simplexml_load_string( $content ,'SimpleXMLElement');
				$feed['channel'] = $this->rssToArray($simpleXmlElement);
				if ((int)$this->feedLimit > 0) {
					$feed['channel']['item'] = array_splice($feed['channel']['item'], 0, $this->feedLimit);
				}
			} catch (HTTP_Request2_MessageException $e) {
				// If we timeout, just move on
				continue;
			}
		}
		return $feed;
	}

	/**
	 * rssToArray RSS to array from a SimpleXMLElement
	 * @param  SimpleXmlElement $simpleXmlElement
	 * @return array
	 */
	private function rssToArray($simpleXmlElement) {
		$rss2Array = array();
		$rss2Array = $this->sxeToArray($simpleXmlElement->channel);
		foreach($simpleXmlElement->channel->item as $simpleXmlElementItem) {
			$simpleXmlElementArray = $this->sxeToArray($simpleXmlElementItem);
			if ($simpleXmlElementArray) {
				$rss2Array['item'][] = $simpleXmlElementArray;
			}
		}
		return $rss2Array;
	}

	/**
	 * sxeToArray Generates the base array for the element, also includes namespaces.
	 * @param  SimpleXMLElement $simpleXmlElement The element to create an array of
	 * @return array
	 */
	private function sxeToArray($simpleXmlElement) {
		$returnArray = false;
		$children = $simpleXmlElement->children();
		$sxeChildrenToArray = $this->sxeChildrenToArray($children);
		if ($sxeChildrenToArray) {
			$returnArray = $sxeChildrenToArray;
		}
		$namespaces = $simpleXmlElement->getNamespaces(TRUE);
		foreach ($namespaces as $ns => $nsuri) {
			$children = $simpleXmlElement->children($ns, true);
			$sxeChildrenToArray = $this->sxeChildrenToArray($children);
			if ($sxeChildrenToArray) {
				$returnArray[$ns] = $sxeChildrenToArray;
			}
		}
		return $returnArray;
	}

	/**
	 * sxeChildrenToArray Returns an array of the elements children and attributes recursively
	 * @param  mixed $children The children of a element
	 * @return array
	 */
	private function sxeChildrenToArray($children) {
		$nodeData = array();
		if (count($children) > 0) {
			foreach ($children as $elementName => $node) {
				$nodeName = $this->stringToArrayKey((string)$elementName);
				$nodeData[$nodeName] = array();
				$nodeAttributes = $node->attributes();
				if (count($nodeAttributes) > 0) {
					foreach ($nodeAttributes as $nodeAttributeName => $nodeAttributeValue) {
						$arrayKey = $this->stringToArrayKey((string)$nodeAttributeName);
						$arrayValue = trim((string)$nodeAttributeValue->__toString());
						$nodeData[$nodeName][$arrayKey] = $arrayValue;
					}
				}
				$nodeValue = trim((string)$node);
				if (strlen($nodeValue) > 0) {
					if (count($nodeAttributes) == 0) {
						$nodeData[$nodeName] = $nodeValue;
					} else {
						$nodeData[$nodeName]['value'] = $nodeValue;
					}
				} else {
					if ($nodeName != 'item') {
						$childs = $this->sxeToArray($node);
						if ($childs) {
							$nodeData[$nodeName] = $childs;
						}
					}
				}
			}
			return $nodeData;
		} else {
			return false;
		}
	}

	/**
	 * stringToArrayKey Returns a string which is ok to use as array key
	 * @param  string $key The array key to check
	 * @return string
	 */
	private function stringToArrayKey($key) {
		return str_replace('.', '_', trim((string)$key));
	}
}